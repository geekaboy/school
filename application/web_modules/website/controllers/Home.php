<?php
//------------[Controller File name : Demo.php ]----------------------//
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();

    }

    public function index()
    {
        $this->main_view();

    }
    public function main_view()
    {
        //@Plugin & Appjs
		$data['plugin'] = array(
            'assets/theme/website/basic/css/main.css'
        );
		$data['appjs'] = array('appjs/website/home/app.js');

		//@VIEW
		$this->load->view('website/theme/header', $data);
		$this->load->view('website/home/index');
		$this->load->view('website/theme/footer');

    }

}//END CLASS
