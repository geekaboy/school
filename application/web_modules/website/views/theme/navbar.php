<nav class="navbar navbar-expand navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#"> หน้าแรก <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    คณะผู้บริหารและบุคลากร
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">คณะผู้บริหาร</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                    <a class="dropdown-item" href="#">บุคลากรฝ่าย xxxx</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">ข่าวประชาสัมพันธ์</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">ภาพกิจกรรม</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">เว็บบอร์ด</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">สมุดเยี่ยม</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">ติดต่อเรา</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-md-0">
            <input class="form-control" type="text" placeholder="ค้นหา...">
        </form>
    </div>
</nav>
