<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="site_url" content="<?php echo site_url(); ?>">
    <meta name="base_url" content="<?php echo base_url(); ?>">

    <title>Project starter</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/theme/website/basic/bootstrap/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/theme/website/basic/plugin/fontawesome/font-awesome.min.css'); ?>">

    <!--JQuery -->
    <script src="<?php echo base_url('assets/theme/website/basic/js/jquery-3.3.1.min.js'); ?>"></script>
    <!--Bootstrap -->
    <script src="<?php echo base_url('assets/theme/website/basic/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/theme/website/basic/bootstrap/bootstrap.min.js'); ?>"></script>
    <!-- serializejson -->
    <script src="<?php echo base_url('assets/theme/website/basic/plugin/serializjson/jquery.serializejson.min.js'); ?>"></script>
    <!-- sweetalert2 -->
    <script src="<?php echo base_url('assets/theme/website/basic/plugin/sweetalert/sweetalert2.all.min.js'); ?>"></script>
    <!-- global fn and variable -->
    <script src="<?php echo base_url('appjs/global.js'); ?>"></script>

    <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
    <style media="screen">
        body, .nav-link{

            font-family: 'Kanit', sans-serif;
        }
    </style>
</head>
<body>
