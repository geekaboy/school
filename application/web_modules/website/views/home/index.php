<main role="main" class="container p-0">
    <div class="row mb-3">
        <div class="col-12 text-center " style="height:200px;">
            <img src="<?php echo base_url('assets/theme/website/images/schoolLogo.png'); ?>" alt="" width="450" class="mt-5">
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <?php $this->load->view('website/theme/navbar'); ?>
        </div>
    </div>
    <div class="row p-3">
        <div class="col-9">
            <div class="row mb-3">
                <div class="col-12">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="https://dummyimage.com/1500x660/1f1f1f/707070&text=Slide01" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="https://dummyimage.com/1500x660/1f1f1f/707070&text=Slide02" class="d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="https://dummyimage.com/1500x660/1f1f1f/707070&text=Slide02" class="d-block w-100" alt="...">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            ข่าวประชาสัมพันธ์
                        </li>
                    </ol>
                </div>
                <div class="col-12">
                    <table class="table table-borderless table-sm" style="font-size:14px;">
                        <tbody>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right text-small">
                        <a href="#">ดูทั้งหมด <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            กิจกรรมต่างๆ
                        </li>
                    </ol>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-4 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x240/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                </small>
                            </p>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="pull-right text-small">
                        <a href="#">ดูทั้งหมด <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            ข่าวจัดซื้อจัดจ้าง
                        </li>
                    </ol>
                </div>
                <div class="col-12">
                    <table class="table table-borderless table-sm" style="font-size:14px;">
                        <tbody>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        <i class="fa fa-circle" aria-hidden="true"></i> This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
                                    </a>
                                </td>
                                <td width="110">05 มิ.ย. 63</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right text-small">
                        <a href="#">ดูทั้งหมด <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            วารสารโรงเรียน
                        </li>
                    </ol>
                </div>
                <div class="col-3 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x160/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text
                                </small>
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-3 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x160/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text
                                </small>
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-3 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x160/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text
                                </small>
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-3 mb-3">
                    <div class="card">
                        <img class="card-img-top" src="https://dummyimage.com/300x160/1f1f1f/707070" alt="Card image cap">
                        <div class="card-block p-2">
                            <p class="card-text">
                                <small>
                                    This is a longer card with supporting text
                                </small>
                            </p>

                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="pull-right text-small">
                        <a href="#">ดูทั้งหมด <i class="fa fa-angle-double-right"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>

        </div>

        <!--RIGHT SIDE-->
        <div class="col-3">
            <div class="row mb-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-primary text-white">
                            <i class="fa fa-user" aria-hidden="true"></i> ผู้บริหาร
                        </div>
                        <div class="card-body">
                            <div class="pr-2 pl-2 pb-2">
                                <img src="https://dummyimage.com/300x400/1f1f1f/707070" class="card-img-top" alt="..." width="100%">
                            </div>
                            <p class="card-text text-center">
                                <strong>Somequick example</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-info text-white">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> เกี่ยวกับโรงเรียน
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">ประวัติโรงเรียน</li>
                            <li class="list-group-item">สัญลักษณ์โรงเรียน</li>
                            <li class="list-group-item">วิสัยทัศน์ / ปรัชญา</li>
                            <li class="list-group-item">พันธกิจ / เป้าหมาย</li>
                            <li class="list-group-item">เพลงโรงเรียน</li>
                            <li class="list-group-item">หลักสูตรที่เปิดสอน</li>
                            <li class="list-group-item">ทำเนียบผู้บริหาร</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-warning text-white">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i> ดาวน์โหลดเอกสาร
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                            <li class="list-group-item">แบบฟอร์มต่างๆฝ่าย xxxx</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg-secondary text-white">
                            <i class="fa fa-link" aria-hidden="true"></i> LINK ต่างๆ
                        </div>
                    </div>
                    <div class="mt-3">
                        <img src="https://dummyimage.com/300x130/1f1f1f/707070" class="card-img" alt="..." width="100%">
                    </div>
                    <div class="mt-3">
                        <img src="https://dummyimage.com/300x130/1f1f1f/707070" class="card-img" alt="..." width="100%">
                    </div>
                    <div class="mt-3">
                        <img src="https://dummyimage.com/300x130/1f1f1f/707070" class="card-img" alt="..." width="100%">
                    </div>
                    <div class="mt-3">
                        <img src="https://dummyimage.com/300x130/1f1f1f/707070" class="card-img" alt="..." width="100%">
                    </div>
                    <div class="mt-3">
                        <img src="https://dummyimage.com/300x130/1f1f1f/707070" class="card-img" alt="..." width="100%">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <footer class="pt-4 my-md-0 pt-md-5 border-top">
        <div class="row">
            <div class="col-3 text-center">
                <img class="mb-2" src="<?php echo base_url('assets/theme/website/images/schoolLogo.png'); ?>" alt="" width="250">
            </div>
            <div class="col-3">
                <h5>Features</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Cool stuff</a></li>
                    <li><a class="text-muted" href="#">Random feature</a></li>
                    <li><a class="text-muted" href="#">Team feature</a></li>
                    <li><a class="text-muted" href="#">Stuff for developers</a></li>
                    <li><a class="text-muted" href="#">Another one</a></li>
                    <li><a class="text-muted" href="#">Last time</a></li>
                </ul>
            </div>
            <div class="col-3">
                <h5>Resources</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Resource</a></li>
                    <li><a class="text-muted" href="#">Resource name</a></li>
                    <li><a class="text-muted" href="#">Another resource</a></li>
                    <li><a class="text-muted" href="#">Final resource</a></li>
                </ul>
            </div>
            <div class="col-3">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="#">Team</a></li>
                    <li><a class="text-muted" href="#">Locations</a></li>
                    <li><a class="text-muted" href="#">Privacy</a></li>
                    <li><a class="text-muted" href="#">Terms</a></li>
                </ul>
            </div>
        </div>
    </footer>
</main>
